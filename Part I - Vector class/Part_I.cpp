#include <cmath>
#include <ostream>
#include <iostream>
#include <iomanip>
#include <initializer_list>
#include <string>
#include <exception>

using namespace std;

template<typename T>
class Vector
{
	public:

	void print(const std::string &info1) const
	{
		// print the address of this instance, the attributes `length` and
		// `data` and the `info` string
		std::cout << "  " << this << " " << length << " " << data << "  " << info1 << std::endl;
	}

	// Default constructor
	Vector()
	{
		length = 0;
		data = nullptr;
	}

	// Constructor with length input
	Vector(int L)
	{
		length = L;
		data = new T[L];
		print("Made 1");
	}

	// Constructor with initializer list. Uses delegation to construct vector with length of this list
	Vector(std::initializer_list<T> L)
		:Vector(L.size())
	{
		for (int n = 0; n < length; n++)
		{
			data[n] = L.begin()[n];
		}
	}

	// Copy operator. Uses delegation to construct a NEW vector with length of the input vector
	Vector(const Vector &v)
		:Vector(v.length)
	{
		for (int n = 0; n < v.length; n++)
		{
			data[n] = v.data[n];
		}
	}

	// Copy assignment operator. Overwrites the left-hand side vector with the data from the right-hand side vector
	Vector& operator=(const Vector &v)
	{
		delete[] data;
		length = v.length;


		for (int n = 0; n < v.length; n++)
		{
			data[n] = v.data[n];
		}
		print("Used: copy assignment operator");
		return *this;
	}

	// Move assignment operator. Overwrites the left-hand side vector with the data from the right-hand side vector
	// Then it deletes the right-hand side vector
	Vector& operator=(Vector &&v)
	{
		delete[] data;
		length = v.length;

		for (int n = 0; n < v.length; n++)
		{
			data[n] = v.data[n];
		}

		v.length = 0;
		v.data = nullptr;

		print("Used: move assignment operator");
		return *this;
	}

	//Attributes of this class
	int length;
	T* data;

	// New type U for the second vector (of different type than T). Automatic return type is enabled.
	// Creates a temporary res vector with automatic return type that stores the sum, then returns this vector as output
	template<typename U>
	auto operator+(const Vector<U> &v)->Vector<decltype(data[0] + v.data[0])>
	{
		if (length != v.length) throw "Cannot add, vectors different size";

		Vector<decltype(data[0] + v.data[0])> res(length);

			for (int n = 0; n < v.length; n++)
			{
				res.data[n] = data[n] + v.data[n];
			}

		print("Used: add operator");
		return res ;
	}

	// Same as for the add operator
	template<typename U>
	auto operator-(const Vector<U> &v)->Vector<decltype(data[0] - v.data[0])>
	{
		if (length != v.length) throw "Cannot subtract, vectors different size";

		Vector<decltype(data[0] - v.data[0])> res(length);

		for (int n = 0; n < v.length; n++)
		{
			res.data[n] = data[n] - v.data[n];
		}

		print("Used: subtract operator");
		return res;
	}

	// New type S for the scalar type. Result type is determined by auto.
	template<typename S>
	auto operator*(const S scalar)->Vector<decltype(scalar*data[0])>
	{
		Vector<decltype(scalar*data[0])> res(*this);

		for (int n = 0; n < this->length; n++)
		{
			res.data[n] *= scalar;
		}

		print("Used: scalar multiplication operator");
		return res;
	}

	// Declare the function as a non-member function by making it a friend (thus allowing more than 1 argument)
//	template<typename S>
//	friend auto operator*(const S scalar, Vector<T> const& v)->Vector<decltype(scalar*v.data[0])>
//	{
//		Vector<decltype(scalar*v.data[0])> res(v.length);
//
//		for (int n = 0; n < v.length; n++)
//		{
//			res.data[n] = v.data[n] * scalar;
//		}
//
//		return res;
//	}
};

	template<typename S,typename T>
	auto operator*(const S scalar, Vector<T> const& v)->Vector<decltype(scalar*v.data[0])>
	{
		Vector<decltype(scalar*v.data[0])> res(v.length);

		for (int n = 0; n < v.length; n++)
		{
			res.data[n] = v.data[n] * scalar;
		}

		return res;
	}


// Placed this function out of the vector class to make it a non-member function
template<typename T>
T dot(const Vector<T>& left, const Vector<T>& right)
{
	if (left.length != right.length) throw "Cannot perform dot product, vectors different size";

	T res = 0;
	for (int n = 0; n < left.length; n++)
	{
		res += left.data[n] * right.data[n];
	}

	return res;
}


int main()
{
	Vector<int> a = { 1, 2, 3, 4 };
	Vector<double> aa = { 1, 2, 3, 4 };
	Vector<double> b = { 1.4,2.4,3.4,4.4 };
	int s1 = 3;

	try
	{
		auto h = a - b;
		auto vec = b*s1;
		auto vec1 = s1*b;

		double dotproduct = dot(aa, b);

		for (auto i = 0; i<4; i++)
			std::cout << vec1.data[i] << std::endl;

		std::cout << dotproduct << std::endl;
	}

	catch (const char* msg)
	{
		std::cerr << msg << std::endl;
	}



	return 0;
}
