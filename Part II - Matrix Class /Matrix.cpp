/**
*\file Matrix.cxx
*
*\author Esteban Marques, Ruben van den Heuvel,  Lijie Pan
*
*
*/

#include <iostream>
#include <map>
#include <typeinfo>
#include "Part_I.cpp"

using namespace std;
template<typename T>
class Matrix
{

public:
    const int rows;
    const int cols;
    map<array<int, 2>,T> data;

//Default Constructor
    Matrix()
        :rows(0),cols(0)
    {
        //map<array<int, 2>,T> data = map<array<int, 2>,T>();
    }

//2 Dimensional Constructor
    Matrix(int row ,int col)
        :rows(row),cols(col)
    {
        map<array<int, 2>,T> data = map<array<int, 2>,T>();
    }
// get an element
    T &operator[] (array<int,2> val)
    {
        return data[val];
    }

    template <typename U>
    auto matvec(const Vector<U> vec)-> Vector<double>
    {
        Vector<decltype(data[{0,0}]+vec.data[0])> res {rows};
        decltype(vec.data[0]+ data[{0,0}]) temp = 0;

        for (int i=0; i<rows; ++i)
        {
            for (int j=0; j<cols; ++j)
            {
                //cout<<data[{i,j}]<<endl;
                //cout<<vec.data[j]<<endl;
                temp = temp+ data[ {i,j}]*vec.data[j];
            };
            res.data[i] = temp;
            temp = 0;
        }
        return res;
    }

};
main()
{

//Matrix<int> M;
    Matrix<double> M {2,4};
    for (int i=0; i<M.rows; i++)
        {
            for (int j=0; j<M.cols; j++)
            {
                M.data[{i,j}] = 1.0;
            }
        }
    //M.print();
    //M[ {0,0}] = 10;
    Vector<double> a = { 1, 2, 3, 4 };
    Vector<double> b = M.matvec(a);
    cout<<b.data[0]<<","<<b.data[1]<<endl;
    //cout<< typeid(M[{0,0}]).name()<<endl;
    //cout<< typeid(1).name()<<endl;

}














